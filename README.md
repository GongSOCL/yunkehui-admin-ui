# 项目概述

* 产品名称：优药后台管理系统ui
* 项目代号：admin-ui
* 官方地址：<http://yy-git.youyao99.com/youyao/admin-ui>

## 运行环境要求

* node 10.0+

## 开发环境部署/安装

请确保本地安装了 [nodejs 环境](https://nodejs.org/zh-cn/)。

### 基础安装

#### 1. 克隆源代码

克隆源代码到本地：

```bash
git clone http://yy-git.youyao99.com/youyao/admin-ui.git
```

#### 2. 添加配置文件

```bash
cp .env.development.example .env.development
```

根据需要修改配置

```bash
VUE_APP_BASE_API = 'http://localhost:9501/api'
```

#### 3. 安装依赖

```bash
npm install --registry=https://registry.npm.taobao.org
```

#### 4. 启动服务

```bash
npm run dev
```

### 链接入口

* 首页地址：<http://localhost:9528/>

## 主要依赖列表

* vuejs：[vuejs：](https://vuejs.org/)
* element-ui：[element-ui](https://element.eleme.cn/#/zh-CN/component/installation)
* vue-element-admin：[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)
