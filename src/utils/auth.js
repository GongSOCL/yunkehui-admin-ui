import Cookies from 'js-cookie'

const TokenKey = 'youyao_admin_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  let isHttps = document.location.protocol == 'https'
  return Cookies.set(TokenKey, token, {
    sameSite: "Strict",
    secure: isHttps ? true : false
  })
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
