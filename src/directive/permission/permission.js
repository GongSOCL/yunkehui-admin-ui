import store from '@/store'

function checkPermission(el, binding) {
  const { value } = binding
  const actions = store.getters && store.getters.permission_actions

  if (value && value !== '') {
    const hasPermission = actions.includes(value)

    if (!hasPermission) {
      el.parentNode && el.parentNode.removeChild(el)
    }
  } else {
    throw new Error(`need actions! Like v-permission="system:user:edit"`)
  }
}

export default {
  inserted(el, binding) {
    checkPermission(el, binding)
  },
  update(el, binding) {
    checkPermission(el, binding)
  }
}
