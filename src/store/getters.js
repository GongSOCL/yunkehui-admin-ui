const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  permission_routes: state => state.permission.routes,
  permission_addRoutes: state => state.permission.addRoutes,
  permission_actions: state => state.permission.actions,
  errorLogs: state => state.errorLog.logs,
  yy_da_id: state => state.user.yy_da_id,
  role_id: state => state.user.role_id
}
export default getters
