import request from '@/utils/request'

export function getUploadConfig(type, business) {
  return request({
    url: '/upload/config',
    method: 'post',
    data: {
      type,
      business
    }
  })
}

// 添加上传记录
export function addUpload(key, bucket, name, url) {
  return request({
    url: '/upload',
    method: 'put',
    data: {
      key,
      bucket,
      name,
      url
    }
  })
}

export function getUploadToken(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/upload/uploadToken',
    method: 'post',
    data
  })
}

export function batchDelete(id = []) {
  return request({
    url: '/upload/batch-delete',
    method: 'POST',
    data: {
     id
    }
  })
}
