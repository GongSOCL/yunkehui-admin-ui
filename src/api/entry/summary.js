import request from '@/utils/request'

export function searchSummaryArea() {
  return request({
    url: '/etinfo/searchSummaryArea',
    method: 'get'
  })
}

export function searchSummaryAgent(data) {
  return request({
    url: '/etinfo/searchSummaryAgent',
    method: 'post',
    data
  })
}

export function searchSummaryDoctor(data) {
  return request({
    url: '/etinfo/searchSummaryDoctor',
    method: 'post',
    data
  })
}

export function getUserSummary(data) {
  return request({
    url: '/etinfo/getUserSummary',
    method: 'post',
    data
  })
}

export function exportUserSummary(data) {
  return request({
    url: '/etinfo/exportUserSummary',
    method: 'post',
    data
  })
}

export function getAgentSummary(data) {
  return request({
    url: '/etinfo/getAgentSummary',
    method: 'post',
    data
  })
}

export function exportAgentSummary(data) {
  return request({
    url: '/etinfo/exportAgentSummary',
    method: 'post',
    data
  })
}

export function getEntrySummary(data) {
  return request({
    url: '/etinfo/getEntrySummary',
    method: 'post',
    data
  })
}


export function exportEntrySummary(data) {
  return request({
    url: '/etinfo/exportEntrySummary',
    method: 'post',
    data
  })
}
