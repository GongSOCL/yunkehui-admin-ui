import request from '@/utils/request'

export function getparameter() {
  return request({
    url: '/etinfo/parameter',
    method: 'get'
  })
}

export function getDoctorInfo(data) {
  return request({
    url: `/etinfo/doctor-info`,
    method: 'post',
    data
  })
}

//用户列表
export function list(query) {
  return request({
    url: '/etinfo/list',
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: '/etinfo/edit',
    method: 'post',
    data: query
  })
}

export function detail(id) {
  return request({
    url: `/etinfo/detail/${id}`,
    method: 'get'
  })
}

export function deleted(id) {
  return request({
    url: `/etinfo/deleted/${id}`,
    method: 'get'
  })
}

export function infoExport() {
  return request({
    url: `/etinfo/infoExport`,
    method: 'post'
  })
}
