import request from '@/utils/request'

export function fetchRoutes() {
  return request({
    url: '/system/routes',
    method: 'get'
  })
}

export function getRole(id) {
  return request({
    url: `/system/role/${id}`,
    method: 'get'
  })
}

export function getRoles(query) {
  return request({
    url: '/system/roles',
    method: 'get',
    params: query
  })
}

export function createRole(data) {
  return request({
    url: '/system/role',
    method: 'post',
    data
  })
}

export function updateRole(id, data) {
  return request({
    url: `/system/role/${id}`,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: `/system/role/${id}`,
    method: 'delete'
  })
}

// 根据角色ID查询菜单下拉树结构
export function getRoleMenu(id) {
  return request({
    url: `/system/role/menu/${id}`,
    method: 'get'
  })
}

export function updateRoleMenu(id, data) {
  return request({
    url: `/system/role/menu/${id}`,
    method: 'put',
    data
  })
}

export function toggleRoleStatus(id) {
  return request({
    url: `/system/role/toggle-status/${id}`,
    method: 'put',
    timeout: 30000
  })
}