import request from '@/utils/request'

//用户列表
export function list(query) {
  return request({
    url: '/doctor/list',
    method: 'get',
    params: query
  })
}

export function searchDoctor(query) {
  return request({
    url: '/doctor/search',
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: '/doctor/edit',
    method: 'post',
    data: query
  })
}

export function detail(id) {
  return request({
    url: `/doctor/detail/${id}`,
    method: 'get'
  })
}

export function updateStatus(id, status) {
  return request({
    url: `/doctor/update-st/${id}`,
    method: 'post',
    data: {
      status
  }
  })
}

export function updatePWD(id,pwd) {
  return request({
    url: `/doctor/update-pwd/${id}`,
    method: 'post',
    data: {
      pwd
    }
  })
}

export function infoExport(data) {
  return request({
    url: `/doctor/infoExport`,
    method: 'post',
    data
  })
}

export function searchAdminDoctor(query) {
  return request({
    url: '/doctor/search-admin',
    method: 'get',
    params: query
  })
}