import request from '@/utils/request'

export function area() {
  return request({
    url: '/agent/area',
    method: 'get'
  })
}

export function province(data) {
  return request({
    url: '/agent/province',
    method: 'post',
    data
  })
}

export function city(data) {
  return request({
    url: '/agent/city',
    method: 'post',
    data
  })
}

export function searchAgent(query) {
  return request({
    url: '/agent/search',
    method: 'get',
    params: query
  })
}

//用户列表
export function list(query) {
  return request({
    url: '/agent/list',
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: '/agent/edit',
    method: 'post',
    data: query
  })
}

export function detail(id) {
  return request({
    url: `/agent/detail/${id}`,
    method: 'get'
  })
}

export function updateStatus(id, status) {
  return request({
    url: `/agent/update-st/${id}`,
    method: 'post',
    data: {
      status
  }
  })
}

export function updatePWD(id,pwd) {
  return request({
    url: `/agent/update-pwd/${id}`,
    method: 'post',
    data: {
      pwd
    }
  })
}

export function infoExport(data) {
  return request({
    url: `/agent/infoExport`,
    method: 'post',
    data
  })
}
