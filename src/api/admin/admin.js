import request from '@/utils/request'

//用户列表
export function list(query) {
  return request({
    url: '/admin/list',
    method: 'get',
    params: query
  })
}

export function update(query) {
  return request({
    url: '/admin/edit',
    method: 'post',
    data: query
  })
}

export function detail(id) {
  return request({
    url: `/admin/detail/${id}`,
    method: 'get'
  })
}

export function updateStatus(id, status) {
  return request({
    url: `/admin/update-st/${id}`,
    method: 'post',
    data: {
      status
  }
  })
}

export function updatePWD(id,pwd) {
  return request({
    url: `/admin/update-pwd/${id}`,
    method: 'post',
    data: {
      pwd
    }
  })
}

export function syncPwd(data) {
  return request({
    url: `/admin/sync-pwd`,
    method: 'post',
    data
  })
}







