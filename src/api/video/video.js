import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/video/list',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/video/update',
    method: 'post',
    data
  })
}

export function deleteVideo(data) {
  return request({
    url: '/video/deleteVideo',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/video/detail',
    method: 'post',
    data
  })
}

export function auditVideo(data) {
  return request({
    url: '/video/audit',
    method: 'post',
    data
  })
}

export function allTags() {
  return request({
    url: '/video/tags',
    method: 'get'
  })
}

export function exportVideo(data) {
  return request({
    url: '/video/exportVideo',
    method: 'post',
    data
  })
}

export function changeShow(data) {
  return request({
    url: '/video/changeShow',
    method: 'post',
    data
  })
}
