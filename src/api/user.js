import request from '@/utils/request'

export function login(data) {
    return request({
        url: '/auth/login',
        method: 'post',
        data
    })
}

export function logout() {
    return request({
        url: '/user/logout',
        method: 'post'
    })
}

export function getInfo() {
    return request({
        url: '/user',
        method: 'get'
    })
}

export function getMenus() {
    return request({
        url: '/user/menus',
        method: 'get'
    })
}

export function getProfile() {
    return request({
        url: '/user/profile',
        method: 'get'
    })
}

export function resetPassword(data) {
    return request({
        url: '/user/password',
        method: 'put',
        data: data
    })
}

export function updateProfile(data) {
  return request({
    url: `/user/profile`,
    method: 'put',
    data
  })
}

export function avatarUpload(data) {
  return request({
    url: '/user/avatar',
    method: 'put',
    data
  })
}

export function getPlatform() {
  return request({
    url: `/platform`,
    method: 'get'
  })
}
