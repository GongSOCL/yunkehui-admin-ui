import { Notification } from 'element-ui';
import { getToken } from '@/utils/auth'
export default {
    ws: undefined,
    url: "",
    platform: 1,
    token: '',
    client_id: '',
    timeObj: undefined,
    connectWs: function() {
        let token = getToken()
        if(token == undefined) {
            //未登陆
            return;
        }
        token = token.substr(7)
        const platform = 1      //admin标识
        let url = process.env.VUE_APP_WS_BASE_URI
       
        if(url == undefined) {
            return;
        }
        if(!token) {
            return;
        }
        //已经创建连接后不再重连
        if(this.ws != undefined) {
            return;
        }
        let that = this
        let schema = document.location.protocol == 'https:' ? 'wss' : 'ws'
        const ws = new WebSocket(`${schema}://${url}?token=${token}&platform=${platform}`)
        
        //连接成功
        ws.addEventListener("open", function(event) {
            that.onOpen()
        })

        //接收消息事件
        ws.addEventListener("message", function(event) {
            if(event.data == "pong") {
                //心跳包直接跳出
                return;
            }
            let m = JSON.parse(event.data)
            if(typeof m != 'object') {
                //错误格式消息，直接丢弃
                return;
            }
            if(!( 'platform' in m && 'type' in m && 'dir' in m && 'data' in m)) {
                console.log("推送数据格式异常")
                return
            }
            if(m.dir != 2) {
                console.log("数据转发异常")
                return;
            }

            if(m.platform == 0) {
                //ws平台发过来的消息
                that.handleWsPlatformMsg(m)
            } else if(m.platform == 1) {
                //admin后台发过来的消息
                that.handleAdminMsg(m)
            }
        })

        //关闭重连
        ws.addEventListener("close", function(event) {
            console.log("recv closed")
            console.log(event)
            console.log(event.code)
            console.log(event.reason)
            that.onClose()
        })

        //错误
        ws.addEventListener("error", function(event) {
            console.log("ws error:", event.code, event.reason)
        })
        
        this.url = url
        this.platform = platform
        this.token = token
        this.ws = ws
    },
    onOpen: function() {
        console.log("connect to server success!!")
        //TODO: 需要设置心跳
        clearInterval(this.timeObj)
        let that  = this
        this.timeObj = setInterval(() => {
            that.sendPing()
        }, 5000);
        //发送请求记录client_id
        this.sendMsg(true, {}, 100001, 0)
    },
    onClose: function() {
        console.log("connect closed")
        //3秒后关闭连接后重连
        let that = this
        that.ws = undefined
        setTimeout(() => {
            that.connectWs(that.url, that.token, that.platform)
        }, 3000);
    },
    onMsg: function(msg) {
        if(!('biz' in msg)) {
            console.log(msg)
            return;
        }
    },
    onCommand: function(commandId, data = {}) {
            //收到指令
            console.log(commandId)
    },
    sendMsg(isCommand = false, data = {}, commandId = 0, platform = 1) {
        if(this.ws instanceof WebSocket && this.ws.readyState == 1) {
            const msg = {
                platform: platform,
                dir: 1,
                type: isCommand ? 2 : 1,
                data: data,
                command: commandId
            }
            this.ws.send(JSON.stringify(msg))
        } else {
            console.log("ws尚未连接到服务器")
        }
    },
    sendPing() {
        if(this.ws instanceof WebSocket && this.ws.readyState == 1) {
            this.ws.send("ping")
        } else {
            console.log("ws尚未连接到服务器")
        }
    },
    closeWs() {
        if(this.ws == undefined) {
            return;
        }
        this.ws.close()
        this.ws = undefined
    },
    handleWsPlatformMsg(msg) {
        if(msg.type == 1) {
            if(msg.data.type == 1) {
                //业务异常消息
                let errMsg = msg.data.msg
                //业务异常
                Notification({
                    title: '服务异常',
                    message: errMsg,
                    type: 'error'
                })
            }
        } else if(msg.type == 2) {
            let commandId = msg.command
            switch(commandId) {
                case 100001:
                    //client_id查询响应
                    if(msg.data.client_id) {
                        this.client_id = msg.data.client_id
                    }
                break;
            }
        }
    },
    handleAdminMsg(msg) {
        if(msg.type == 1) {
            this.handleAdminBizMsg(msg.data)
        } else if(msg.type == 2) {
            this.handleAdminCommand(msg.command, msg.data)
        }
    },
    handleAdminBizMsg(data) {
        //处理admin下发业务消息
        if(!('biz' in data)) {
            return;
        }
        switch(data.biz) {
            case 1:
            //通知消息
            this.handleNotice(data)
            break;
        }
    },
    handleAdminCommand(commandId, data = {}) {
        //处理admin下发指令消息
    },
    handleNotice(msg) {
        // {
        //     code: xxx,
        //     msg: 'xxx',
            // type: 'info'
            // title: 'xxx',
        // }
        if(!('code' in msg && 'msg' in msg)) {
            console.log("下发响应消息格式错误")
            return;
        }
        let errCode = msg.code
        let errMsg = msg.msg
        let title = '通知'
        if ('title' in msg) {
            title = msg.title
        }
        let type = 'info'
        if ('type' in msg) {
            type = msg.type
        }
        console.log(type)
        //业务通知
        Notification({
            title: title,
            message: errMsg,
            type: type
        })
    }
}